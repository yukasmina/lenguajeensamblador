;Angel Favian Minga
;Operaciones Dianmicas 
;22 de junio 2020

%macro imprimir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h

%endmacro
%macro leer 2
    mov eax,3		
	mov ebx,2		
	mov ecx,%1	
	mov edx,%2    
	int 80H

%endmacro

section .data
        ;___________________Suma_______________
    num1 db "Ingrese el numero 1: "
    len_num1 equ $-num1
    num2 db "Ingrese el numero 2: "
    len_num2 equ $-num2
    msg_suma db "La suma de los dos numero es: "
    len_suma equ $-msg_suma

        ;_____________________Resta________________
    msg_resta db "La Resta de los dos numero es: "
    len_resta equ $-msg_resta


        ;_____________________Multiplicacion________________
    msg_mult db "La Multiplicacion de los dos numero es: "
    len_mult equ $-msg_mult

       ;_____________________Divicion________________ 

    msg_resd db "El residuo de los dos numero es: "
    len_resd equ $-msg_resd
    msg_cos db "El cosiente de los dos numero es: "
    len_cos equ $-msg_cos
    new_line db "",10
    len_new equ $-new_line
section .bss
    numero1 resb 1
    numero2 resb 1
    suma resb 1
    resta resb 1
    multi resb 1
    residuo resb 1
    cosiente resb 1
section .text
    global _start

_start:

;__________________Leer datos ingresados____
    imprimir num1, len_num1
    leer numero1,2
    imprimir num2, len_num2
    leer numero2,2

;_____________________Suma_________________
   
    ;valores covertidos en digitos
    mov al,[numero1]
    mov bl,[numero2]
    sub al,'0'
    sub bl,'0'
    ;** operacion suma
    add al,bl
    add al,'0'
    mov [suma], al

    imprimir msg_suma, len_suma
    imprimir suma,1
    imprimir new_line, len_new
    
   ;________________resta____________________
   
    ;valores covertidos en digitos
    mov al,[numero1]
    mov bl,[numero2]
    sub al,'0'
    sub bl,'0'
    ;****operacion rsta
    sub al,bl
    add al,'0'
    mov [resta], al

    imprimir msg_resta, len_resta
    imprimir resta,1
    imprimir new_line, len_new

    ;_________________Multiplicacion_________________
    mov al,[numero1]
    mov bl,[numero2]
    sub al,'0'
    sub bl,'0'
    ;****operacion mult
    mul bl
    add al,'0'
    mov [multi], al

    imprimir msg_mult, len_mult
    imprimir multi,1
    imprimir new_line, len_new
    
    ;________________Division________________________

    mov al,[numero1]
    mov bh,[numero2]
    sub al,'0'
    sub bh,'0'
    ;****operacion div
    div bh
    add al,'0'
    mov [cosiente],al
    add ah,'0'
    mov [residuo], ah

    ;______cosiente
    imprimir msg_cos, len_cos
    imprimir cosiente,1
    imprimir new_line, len_new	
    ;________Residuo
    imprimir msg_resd, len_resd
    imprimir residuo,1
    imprimir new_line, len_new

   

    mov eax, 1						 ; salida del programa, system exit, sys_exit
	int 80H	