;Angel Favian Minga
;Resta de dos numeros estaticos
section .data
        mensaje_presentacion db "La division de los dos numero es: ",10
        len_msg_presentacion equ $-mensaje_presentacion
        residuo_msg db "La sobrante de la divsion es: ",10
        len_residuo equ $-mensaje_presentacion
        new_line db "",10
        len_new equ $-new_line
section .bss
        residuo resb 1
        cosiente resb 1
section .text
        global _start

_start:
;____________________________Caso UNo______________
;al cociente
;ah residuo
    mov al,9
    mov bh,2
;************************************* operacion
    DIV bh
    add al,'0'
    mov [cosiente],al

    add ah,'0'
    mov [residuo], ah
;__________________________________Caso Dos___________

;************************************Imprimir mensaje de presentacion********************************************
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,mensaje_presentacion		; 
	mov edx,len_msg_presentacion		; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux

    ;************************************imprime numero********************************************
    mov eax,4		; tipo de sub-rutina, para lecctura
	mov ebx,1		; tipo de estandar  de entrada
	mov ecx,cosiente		; Lo almacena en numero, lo que captura en el teclado
	mov edx,1          ;es el numero de reserva, nnumero de caracteres
	int 80H	   		; interrupción de software para el sistema operativo linux
;************************************Imprimir mensaje de presentacion********************************************
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,residuo_msg		; 
	mov edx,len_residuo		; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux

    ;************************************imprime numero********************************************
    mov eax,4		; tipo de sub-rutina, para lecctura
	mov ebx,1		; tipo de estandar  de entrada
	mov ecx,residuo		; Lo almacena en numero, lo que captura en el teclado
	mov edx,1          ;es el numero de reserva, nnumero de caracteres
	int 80H	   		; interrupción de software para el sistema operativo linux

;************************************Imprimir mensaje de presentacion********************************************
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,new_line		; 
	mov edx,len_new		; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux
    
    mov eax, 1						 ; salida del programa, system exit, sys_exit
	int 80H	