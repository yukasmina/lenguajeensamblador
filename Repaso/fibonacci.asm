;Angel Minga
;loop Cuadrado

%macro imprimir 1
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, 1
	int 80h
%endmacro
section	.data
	msj db '*'
	new_line db 10,''
section .bss
        aux resb 1
        num1 resb 1
        num2 resb 1
section	.text
   global _start        ;must be declared for using gcc
	
_start:
	mov eax, 0 ;a
	add eax,'0'
	mov ebx, 1 ;b
	add ebx,'0'
	mov [num1], eax
	mov [num2], ebx
	imprimir num1
	imprimir num2
	jmp principal

principal:
	push rbx
	push rax
	mov eax,ebx        ;eax=eax+ebx
    add eax,'0'
	mov [aux],eax
	imprimir aux
for1:
    pop rbx
    inc rbx
 	cmp rbx, 5
	jz salir
	jmp principal
   	
salir:
	mov eax,1
	int 80h