; programa que imprime asteriscos
section	.data
   asterisco db '*'

section	.text
   global _start        ;must be declared for using gcc
	
_start:	
    mov ecx, 9

imprimir:
    dec ecx
    push rcx
    mov eax, 4
	mov ebx, 1
	mov ecx, asterisco   ;ecx remplaza 9  por *
	mov edx, 1
	int 80h

    pop rcx
    cmp ecx,0
    jnz imprimir
    ;jmp imprimir

	mov eax,1             ;sys_exit
	int 80h              
