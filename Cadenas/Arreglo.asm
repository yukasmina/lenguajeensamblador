%macro imprimir 1
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, 1
	int 80h
%endmacro
section .data
	arreglo db 3,2,0,7,5
    len_arreglo equ $-arreglo
    new_line db 10,''
section .bss
    num resb 1
section .text
   global _start        
_start:	  
    mov esi,arreglo ;esi=fijar el tamaño del arreeglo, posicionar el areglo en memoria
    mov edi,0 ;edi= contener el idice del arreglo

impri:
    mov al,[esi]
    add al,'0'
    mov [num],al
    add esi,1
    add edi,1           ;[edi]
    imprimir num
    imprimir new_line
    cmp edi,len_arreglo ; cmp 3,4=> activa carry
                         ; cmp 4,3=> desactiva carry y cero
                         ; cmp 3,3=> desactiva carry y cero se activa
    jmp impri    ; se ejecuta cuando la vandera de carry esta activa


salir:
	mov eax, 1
	int 80h
    
