%macro imprimir 2
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, %2
	int 80h
%endmacro
section .data
    msj1 db "Ingrese 5 numeros",10
    len1 equ $-msj1
    msj2 db "Numero mayor es: "
    len2 equ $-msj2
	arreglo db 0,0,0,0,0
    len_arreglo equ $-arreglo
    new_line db 10,''
section .bss
    num resb 2
section .text
   global _start        
_start:	  
    ;mov esi,arreglo ;esi=fijar el tamaño del arreeglo, posicionar el areglo en memoria
    ;mov edi,0 ;edi= contener el idice del arreglo
    jmp principal

principal:
    mov esi, arreglo
    mov edi,0
    imprimir msj1,len1
leer:
;:::::::::::::::::::::::lectura de datos en el arreglo::::::::::::::::::::::::
    mov eax,3		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,0		; tipo de estandar
	mov ecx,num		; 
	mov edx,2		; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux

    mov al,[num]
    sub al,'0'

    mov [esi],al    ; movemos el valor a un indice del arreglo

    inc edi
    inc esi   ; indice del arreglo

    cmp edi, len_arreglo
    jb leer

    mov ecx,0       ; controlador de la comparacion
    mov bl,0

comparacion:
    mov al,[arreglo+ecx]
    cmp al,bl
    jl contador
    mov bl,al
contador:
    inc ecx
    cmp ecx,len_arreglo
    jl comparacion

presentar:
    add bl,'0'
    mov [num],bl
    imprimir msj2,len2
    imprimir num, 1
salir:
	mov eax, 1
	int 80h
    