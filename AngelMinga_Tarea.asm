;Comentar el significado de cada línea
;Los macros sirven para:
;-Ahorrar código cuando se quiere hacer un proceso repetitivo.
;-Menos errores al momento de realizar código repetitivo.
;-Permite hacer uso de variables que son pasados como parámetros.
;-Mayor seguridad en el código, se pueden invocar desde un archivo externo al programas ASM.

; permite la llamada varias veces  ya que cadaves que se quisiera imprmir una cadena se
;escribiria imprime longitud, cadena.
;......................................................................................................
; Las macros son más rápidas que las subrutinas (pues ahorran dos saltos,
; uno de ida y uno de vuelta, más los tejemanejes de la pila),

%macro imprimir 2 ;define el macro que es imprmir; macro nombre y numero de argumentos
	mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,%1 		; Se carga EL PRIMER ARGUMENTO (la direccion) en ecx
	mov edx,%2		; Se carga EL SEGUNDO ARGUMENTO EN edx
	int 80H	   		; interrupción de software para el sistema operativo linux
%endmacro ; fin del macro imprimir
 
%macro leer 2 		;define el macro leer: fragmentos de codigo
    mov eax,3		; tipo de sub-rutina, operación de escritura=>salida
    mov ebx,0		; tipo de estandar
    mov ecx,%1 		; Se carga EL PRIMER ARGUMENTO (la direccion) en ecx
    mov edx,%2		; Se carga EL SEGUNDO ARGUMENTO EN edx
    int 80H 	    ; interrupción de software para el sistema operativo linux
%endmacro	;fin del macro leer


; ecx,modo de acceso
; edx, permisos
section .bss		; conteniendo datos no inicializados
	;instrucción resb, la cual reserva  Byte o bytes no inicializado.
	; El problema es que no se dispone de una identificación de esa memoria que se acaba de reservar
	auxiliar resb 30	; la variable (auxiliar) reserva 30 bytes 
	auxiliarb resb 30	; la variable (auxiliarb) reserva 30 bytes 
	auxiliarc resb 30	; la variable (auxiliarc) reserva 30 bytes 


section .data		; conteniendo datos inicializados
	msg db 0x1b ,"       " ; 6 espacios para contener al dato
	lenmsg equ $-msg	   ; constante de longuitud que calcula el # caracteres de msg



	salto db " ",10 		; 1 espacio para contener al dato de salto
	lensalto equ $-salto	; constante de longuitud que calcula el # caracteres que tiene salto




section .text
    global _start    		; etiqueta global que marca el comienzo del programa
_start:
	
	mov ecx,9				;numero de beces que se procesara

	mov al,0				;mueve el contenido de la localidad 0 a al
	mov [auxiliar],al		;mueve el contenido de al a auxliar

cicloI:
	push ecx				;preserva ecx en la pila
	mov ecx,9				;numero de beces que se procesara

	mov al,0				;mueve el contenido de la localidad 0 a al
	mov [auxiliarb],al		; mueve el valor de   al   a auxiliarb

	cicloJ:					; Inicio del cicloJ
		push ecx			;preserva ecx en la pila


		call imprimir0al9   ; llama a la subrutina  imprimir0al9
		; imprimir msg2,lenmsg2

	fincicloJ:	; finaliza el cicloJ

		mov al,[auxiliarb]			;mueve el contenido de a al a auxliar
		inc al						; al incrementa en 1
		mov [auxiliarb],al			;mueve el contenido de al a auxliarb

		pop ecx						; recupera ecx de la pila
		loop cicloJ					;decrementa ecx, si ecx!=0
		
	;imprimir salto,lensalto

fincicloI:
	mov al,[auxiliar]				;mueve el contenido de auxliar a al
	inc al							; al incrementa en 1
	mov [auxiliar],al				;mueve el contenido de al a auxliar

	pop ecx							; recupera ecx de la pila
	loop cicloI						;decrementa ecx, si ecx!=0
	

salir:
	mov eax, 1						 ; salida del programa, system exit, sys_exit
	int 80H							; interrupción de software para el sistema operativo linux



imprimir0al9:
	
	mov ebx,"["						; guarda "[" en ebx
	mov [msg+1], ebx				;almacena el contenido de ebx con el direccionamiento indexado en 1 byte despues en [msg+1]  

	mov bl,[auxiliar]				;copiar valor de auxiliar a bl
	add bl,'0'						;sumar el contenido '0' a bl
	mov [msg+2], bl					;almacena el contenido de bl con el direccionamiento indexado en 2 byte despues en [msg+2]  


	mov ebx,";"						; guarda "[" en ebx
	mov [msg+3], ebx				;almacena el contenido de ebx con el direccionamiento indexado en 3 byte despues en [msg+3]  

	
	mov bl,[auxiliarb]				;copiar valor de auxiliar a bl
	add bl,'0'						;sumar el contenido '0' a bl
	mov [msg+4],bl					;almacena el contenido de bl con el direccionamiento indexado en 4 byte despues en [msg+4]  

	mov ebx,"fJ"					; guarda "fj" en ebx
	mov [msg+5], ebx				;almacena el contenido de ebx con el direccionamiento indexado en 5 byte despues en [msg+5]  

	imprimir msg,lenmsg				; llama a macros imprimir pasando los dos argumentos (msg, lenmsg)

	ret								;retorno


