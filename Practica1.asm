section .data ;agrupa las constantes 
    mensaje db "Mi primera vez con NASM"  ; constante mensaje de un byte en momoria
    longitud EQU $-mensaje                ; constante de longuitud que calcula el # caracteres de mensaje 


section .text  ;
    global _start
_start:
    ;********************imprimir el mensaje*********************

    mov eax, 4              ; tipo de sub-rutina, operación de escritura=>salida
    mov ebx, 1              ; tipo de estandar, por teclado
    mov ecx, mensaje        ; El registro ecx se almacena la refeencia a imprimif "mnsaje"
    mov edx, longitud       ; El registro edx se almacena la refeencia a imprimi por # de caracteres "mnsaje"
    int 80H                 ; interrupción de software para el sistema operativo linux

    mov eax, 1              ; salida del programa, system exit, sys_exit
    mov ebx, 0              ; si el retorno es 0 (200 en la web) ok
    int 80H

    